dotfiles
========

Introduction
------------
My dot files, except for cryptographical keys (SSH, GPG).

I'm starting with the VIM ones.

Oh, and the "install" script is there to link the stuff from wherever the repo
is to your home directory.
